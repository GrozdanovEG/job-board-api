<?php

namespace App\Entity;

use App\Repository\ApplicantRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ApplicantRepository::class)]
class Applicant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::OBJECT)]
    private ?object $contactInfo = null;

    #[ORM\Column(type: Types::OBJECT, nullable: true)]
    private ?object $jobPreferences = null;

    #[ORM\Column(type: Types::OBJECT, nullable: true)]
    private ?object $jobsApplied = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContactInfo(): ?object
    {
        return $this->contactInfo;
    }

    public function setContactInfo(object $contactInfo): self
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    public function getJobPreferences(): ?object
    {
        return $this->jobPreferences;
    }

    public function setJobPreferences(?object $jobPreferences): self
    {
        $this->jobPreferences = $jobPreferences;

        return $this;
    }

    public function getJobsApplied(): ?object
    {
        return $this->jobsApplied;
    }

    public function setJobsApplied(?object $jobsApplied): self
    {
        $this->jobsApplied = $jobsApplied;

        return $this;
    }
}
