<?php

namespace App\Entity;

use App\Repository\JobRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JobRepository::class)]
class Job
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::OBJECT)]
    private ?object $requiredSkills = null;

    #[ORM\Column(type: Types::OBJECT)]
    private ?object $experience = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array $applicants = [];

    #[ORM\Column(type: Types::OBJECT)]
    private ?object $company = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRequiredSkills(): ?object
    {
        return $this->requiredSkills;
    }

    public function setRequiredSkills(object $requiredSkills): self
    {
        $this->requiredSkills = $requiredSkills;

        return $this;
    }

    public function getExperience(): ?object
    {
        return $this->experience;
    }

    public function setExperience(object $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getApplicants(): array
    {
        return $this->applicants;
    }

    public function setApplicants(?array $applicants): self
    {
        $this->applicants = $applicants;

        return $this;
    }

    public function getCompany(): ?object
    {
        return $this->company;
    }

    public function setCompany(object $company): self
    {
        $this->company = $company;

        return $this;
    }
}
