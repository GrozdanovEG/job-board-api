<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $location = null;

    #[ORM\Column(type: Types::OBJECT, nullable: true)]
    private ?object $jobPosts = null;

    #[ORM\Column(type: Types::OBJECT)]
    private ?object $contactInformation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getJobPosts(): ?object
    {
        return $this->jobPosts;
    }

    public function setJobPosts(?object $jobPosts): self
    {
        $this->jobPosts = $jobPosts;

        return $this;
    }

    public function getContactInformation(): ?object
    {
        return $this->contactInformation;
    }

    public function setContactInformation(object $contactInformation): self
    {
        $this->contactInformation = $contactInformation;

        return $this;
    }
}
