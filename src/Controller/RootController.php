<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RootController extends AbstractController
{
    #[Route('/', name:'app_root_path')]
    public function root(): Response
    {
        $response = ["message" => 'application loaded'];
        return $this->json(
            $response,
            '200');
    }
}